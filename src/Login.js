//import logo from './logo.svg';
import './App.css';
import { Navbar, Row } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './Login.css';
import { useState } from 'react';

function Login() {
  const [UserName,setUserName]=useState("");
  const [password,setPassword]=useState("");

  // const handleSubmit = (event) => {
  //   event.preventDefault();
  //   let name = this.event.target.UserName;
  //   let pwd = this.event.target.password;
  //   if (name === "") {
  //     alert("Please enter username");
  //   }
  //   else if (pwd === "") {
  //     alert("password should not be empty");
  //   }
  //   else
  //   {
  //     alert("success");
  //   }
  // }

  return (
  <div>
  <Navbar bg="dark" variant="dark">
    <Navbar.Brand href="#home">
      <img
        alt=""
        src="./logo1.svg"
        width="50"
        height="20"
        className="d-inline-block align-top"
      />{'ALBENUS'}
      
    </Navbar.Brand>
  </Navbar>

<h2>LogIn Form</h2>

<p>
  <label>UserName : <input type="text" value={UserName} onChange={e=>setUserName(e.target.value)} 
                    name="username"></input></label>
</p>
<br/>

<p>
  <label>PassWord : <input type="password" value={password} onChange={e=>setPassword(e.target.value)} 
                    name="password"></input></label>
</p>

<Row>
   <div className="image-holder">
					<img src="./image.jpg" alt=""/>
	  </div>
    </Row>
    <div className="btn btn-primary" type="submit"  onClick={e=>alert("success")}>Login</div>
</div>
  );
}

export default Login;


